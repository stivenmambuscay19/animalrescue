﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salir : MonoBehaviour{
    public void SalirJuego(){
        // mensaje en consola
        Debug.Log("Saliendo del Juego");
        //Metodo para salir de la aplicacion
        Application.Quit();
    }
}
