﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class RecogerGema : MonoBehaviour{

    public GameController controladorJuego;
    

    // Start is called before the first frame update
    void Start()
    {


    }


    void OnTriggerEnter(Collider objetoQueColisiona){
        //Vamos a destruir objetos y sumarlos a la coleccion
        if (objetoQueColisiona.tag == "ManoDerecha"){
            // Destruir este objeto y sumar a la coleccion
            controladorJuego.numeroGemas++;
            controladorJuego.publicarColecciones();
            //Destruyo el Objeto
            Destroy(gameObject, 1f);
        }


    }

    // Update is called once per frame
    void Update()
    {


    }


}
