﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bloqueadorparque : MonoBehaviour
{
    public Text mensajes;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "Debes encontrar al gato amarillo";
        }
    }

    void OnTriggerExit(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "";
        }

    }
}
