﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirPuerta : MonoBehaviour
{
    public GameObject puerta;
    Animator animatorPuerta;
    // Start is called before the first frame update
    void Start()
    {
        animatorPuerta = puerta.GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Piezquierdo")
        {
            animatorPuerta.SetTrigger("AbrirPuerta");
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
