﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // referencia al controlador de animaciones.
    private Animator animator;
    // determinamos si está avanzando o  está quieto
    private bool avanzando;

    //variables relacionadas con el movimiento del personaje
    public float velocidad = 3.0f;
    // para las funciones relacionadas con físicas
    // debo garantizar el acceso al Rigid body del personaje
    private Rigidbody rb;
    // definición de un vector direccion movimiento
    Vector3 direccion;

    // nivel de vida
    public int vida = 100;
    public Slider sliderVida;

    public Text mensajes;

    void Start()
    {
        // es recomendable en este metodo configurar la inicialización
        // de nuestras variables de objetos de juego.

        // accedemos al rigid body
        rb = GetComponent<Rigidbody>();
        // acceder al componente de animaciones "ejecute x animación"
        animator = GetComponent<Animator>();
    }

    public void QuitarVida(int perdidaVida)
    {
        vida = vida - perdidaVida;
        Debug.Log("nivel de vida >>> " + vida);
    }

    public void ActualizarVida()
    {
        sliderVida.value = (float)vida;
    }

    public IEnumerator Morir()
    {
        animator.SetTrigger("Morir");
        mensajes.text = "HAS MUERTO !";
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("Nivel 1");
    }

    // se ejecuta cada frame SIN VALIDACIONES
    // no se recomienda programar los comportamientos físicos
    void Update()
    {
        ActualizarVida();
        if (vida <= 0)
        {
            StartCoroutine("Morir");
        }
    }

    // se ejecuta en cada frame PERO VALIDA SU EJECUCIÓN.
    // este es el método ideal para programar eventos relacionados
    // con las físicas de juego.
    void FixedUpdate()
    {
        // CUANDO REQUIERO UNA ACCIÓN ESPECIFICA 
        // SOLO DEBO CONFIGURAR CON QUE INPUT DISPARO EL EVENTO
        // EN EL ANIMATOR CONTROLLER
        if (Input.GetKeyDown(KeyCode.G))
        {
            Debug.Log("levantar");
            animator.SetTrigger("Levantar");
        }
        // para patear
        if (Input.GetButton("Fire2"))
        {
            animator.SetTrigger("Patada");
        }

        if (Input.GetButton("Fire1"))
        {
            animator.SetTrigger("Soltar");
        }

        //Saltar
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Saltar");
            animator.SetTrigger("Saltar");
        }

        /* // input para apagar una luz
         if (Input.GetKeyDown(KeyCode.T))
         {
             GameObject.Find("luz").GetComponent<Light>().enabled = false;
         }
         if (Input.GetKeyDown(KeyCode.R))
         {
             GameObject.Find("luz").GetComponent<Light>().enabled = true;
         }
 */

        // identifico las variables de entrada de usuario.
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        // en cada frame valido el movimiento del personaje
        Movimiento(horizontal, vertical);
        // EJECUTO LA ANIMACIÓN CORRESPONDIENTE AL MOVIMIENTO -> CAMINAR
        Animacion(horizontal, vertical);
    }

    public void Animacion(float horizontal, float vertical)
    {
        //Debug.Log("horizontal >>> " + horizontal + " / vertical >>> " + vertical);
        avanzando = horizontal != 0f || vertical != 0f;
        //Debug.Log("avanzando >>> " + avanzando);
        animator.SetBool("Walk", avanzando);
    }

    public void Movimiento(float horizontal, float vertical)
    {
        // se define un vector relacionado con la dirección hacia la
        // cual se mueve el personaje
        direccion.Set(horizontal, 0f, vertical);
        direccion = direccion.normalized * velocidad * Time.deltaTime;
        // validar el rigidbody asignado al personaje
        if (rb)
        {
            if (horizontal > 0.0f)
            {
                rb.transform.Translate(
                    Vector3.right * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (horizontal < 0.0f)
            {
                rb.transform.Translate(
                    Vector3.left * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (vertical > 0.0f)
            {
                rb.transform.Translate(
                    Vector3.forward * velocidad * Time.deltaTime
                );
            }
            if (vertical < 0.0f)
            {
                rb.transform.Translate(
                    Vector3.back * velocidad * 0.5f * Time.deltaTime
                );
            }
        }
    }
}
