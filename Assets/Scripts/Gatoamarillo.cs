﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Gatoamarillo : MonoBehaviour
{
    public Text mensajes;
    public GameObject Bloqueadorparque;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            //la venta de las corruntinas es que se puede poner pequeños
            //temporizadores para controlar los momentos
            StartCoroutine("GatoEncontrado");
        }
    }

    public IEnumerator GatoEncontrado()
    {
        mensajes.text = " Moou ";
        yield return new WaitForSeconds(1.0f);
        mensajes.text = "";
        Destroy(Bloqueadorparque, 1.0f);
    }
}