﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameController : MonoBehaviour{

    public int numeroGemas;
    public int numeroMascotas;
    public Text TextGemas;
    public Text TextMascotas;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void publicarColecciones(){
        //Debug. Log permite visualizar texto en la consola
        // se utiliza mucho para prototipar y monitorear
        Debug.Log("numero gemas -->>>" + numeroGemas);
        Debug.Log("numero monedas -->>>" + numeroMascotas);
        TextGemas.text = "Numero Gemas: " + numeroGemas;
        TextMascotas.text = "Numero Monedas: " + numeroMascotas;
    }

    void Update()
    {
        
    }
}
