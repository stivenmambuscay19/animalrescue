﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecogerMascotas : MonoBehaviour
{
    public GameController controladorJuego;
    // Start is called before the first frame update
    void Start()
    {

    }
    void OnTriggerEnter(Collider objetoQueColisiona)
    {

        if (objetoQueColisiona.tag == "ManoDerecha")
        {
            //Vamos a destruir objetos y sumarlos a la coleccion
            controladorJuego.numeroMascotas++;
            controladorJuego.publicarColecciones();
            //Destruyo el objeto
            Destroy(gameObject, 0.05f);
        }

    }
    // Update is called once per frame
    void Update()
    {

    }
}