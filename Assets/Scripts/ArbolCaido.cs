﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArbolCaido : MonoBehaviour
{
    // Start is called before the first frame update
    // las variables publicas se previsualizan en el inspector
    public GameObject arbol;
    Animator arbol_anim;


    void Start()
    {
        // Para obtener acceso a los componentes (objetos) de un
        //Gameobjet utilizo el metodo getComponent
        arbol_anim = arbol.GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    {


    }
    //Utilizo el metodo onTriggerEnter para detectar una colision
    void OnTriggerEnter(Collider objetoColisiona)
    {
        if (objetoColisiona.tag == "Player")
        {
            arbol_anim.SetTrigger("TroncoCaid");
        }


    }



}
