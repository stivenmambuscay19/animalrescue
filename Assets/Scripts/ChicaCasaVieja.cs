﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChicaCasaVieja : MonoBehaviour
{
    public Text mensajes;
    public GameObject bloqueador;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            //la venta de las corruntinas es que se puede poner pequeños
            //temporizadores para controlar los momentos
            StartCoroutine("ChicaEncontrada");
        }
    }

    public IEnumerator ChicaEncontrada()
    {
        mensajes.text = "Estan situados en los alrededores del parque ";
        yield return new WaitForSeconds(2.0f);
        mensajes.text = "";
        Destroy(bloqueador, 1.0f);
    }
}